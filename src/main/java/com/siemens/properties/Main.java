package com.siemens.properties;

public class Main {

    public static void main(String[] args) {
        PropertiesValidation propertiesValidation = new PropertiesValidation();
        propertiesValidation.setProperty("a", "5");
        propertiesValidation.setProperty("b", "10");
        ValidationProperty a = propertiesValidation.getPropertyValidation("a");
        ValidationProperty b = propertiesValidation.getPropertyValidation("b");

        a.isNumber().minValue(1).maxValue(100).betweenValues(1, 100).isEqualTo("5");
        propertiesValidation.assertTrue(
                () -> a.minValue(1),
                () -> b.maxValue(10)
        );

        a.isNumber().minValue(1).maxValue(100).betweenValues(1, 100).isEqualTo("5");
        propertiesValidation.assertIfNot(() -> a.isEqualTo("1")).then(() -> b.maxValue(10));
        propertiesValidation.assertIf(() -> a.isEqualTo("5")).then(() -> b.maxValue(10));
        propertiesValidation.assertIf(() -> a.isEqualTo("5")).thenNot(() -> b.isEqualTo("10"));
    }
}

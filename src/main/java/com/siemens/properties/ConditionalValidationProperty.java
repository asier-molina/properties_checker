package com.siemens.properties;

import java.util.function.Supplier;

public class ConditionalValidationProperty {

    private Supplier<ValidationProperty> originalCondition;

    public ConditionalValidationProperty() {
    }

    public ConditionalValidationProperty(Supplier<ValidationProperty> originalCondition) {
        this.originalCondition = originalCondition;
    }

    public void then(Supplier<ValidationProperty> condition) {
        if (originalCondition != null) {
            condition.get();
        }
    }

    public void thenNot(Supplier<ValidationProperty> condition) {
        boolean returnOk = false;
        if (originalCondition != null) {
            try {
                condition.get();
                returnOk = true;
            } catch (Exception exception) {
                // Not problem if exception is thrown
            }
            if (returnOk) {
                throw new SiemensPropertiesException();
            }

        }
    }
}

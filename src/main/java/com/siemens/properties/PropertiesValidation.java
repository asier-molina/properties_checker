package com.siemens.properties;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Map;
import java.util.Properties;
import java.util.function.Supplier;

/**
 * Class that warps the properties and allows to generate validation conditions in the properties.
 *
 * @author asier_molina
 */
public class PropertiesValidation extends Properties {

    /**
     * Load properties with reader.
     *
     * @param reader reader
     * @throws IOException when reading exception.
     */
    @Override
    public synchronized void load(Reader reader) throws IOException {
        super.load(reader);
        printProperties();
    }

    /**
     * Load properties with inputstream.
     *
     * @param inStream reader
     * @throws IOException when reading exception.
     */
    @Override
    public synchronized void load(InputStream inStream) throws IOException {
        super.load(inStream);
        printProperties();
    }

    private void printProperties() {
        for (Map.Entry<Object, Object> entry : entrySet()) {
            //System.out.println("{}:{}", entry.getKey(), entry.getValue());
        }
    }

    /**
     * Gets a property validation which allows property specific validations.
     *
     * @param key proprety name
     * @return property validation class
     */
    public ValidationProperty getPropertyValidation(String key) {
        return new ValidationProperty(super.getProperty(key));
    }

    /**
     * Gets a property validation which allows property specific validations, assigning a default value if not found.
     *
     * @param key proprety name
     * @return property validation class
     */
    public ValidationProperty getPropertyValidation(String key, String defaultValue) {
        return new ValidationProperty(super.getProperty(key, defaultValue));
    }

    /**
     * Defines that all passes conditions must be true.
     *
     * @param conditions that must be true
     */
    public void assertTrue(
            Supplier<ValidationProperty>... conditions) {
        for (Supplier<ValidationProperty> supplier : conditions) {
            supplier.get();
        }
    }
    
    public ConditionalValidationProperty assertIf(
            Supplier<ValidationProperty> condition) {
        try {
            condition.get();
            return new ConditionalValidationProperty(condition);
        } catch (Exception exception) {
            return new ConditionalValidationProperty();
        }

    }

    public ConditionalValidationProperty assertIfNot(
            Supplier<ValidationProperty> condition) {
        try {
            condition.get();
            return new ConditionalValidationProperty();
        } catch (Exception exception) {
            return new ConditionalValidationProperty(condition);
        }
    }
}

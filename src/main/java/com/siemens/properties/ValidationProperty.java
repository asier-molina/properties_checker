package com.siemens.properties;

public class ValidationProperty {
    private final String propertyValue;

    public ValidationProperty(String value) {
        this.propertyValue = value;
    }

    public ValidationProperty isEqualTo(String value) {
        if (!propertyValue.equals(value)) {
            return throwException();
        }
        return this;
    }

    public ValidationProperty isEqualToInt(Integer value) {
        Integer propertyLong = isNumeric();
        if (!propertyValue.equals(value)) {
            return throwException();
        }
        return this;
    }

    public ValidationProperty isEqualToRegex(String value) {
        if (!propertyValue.matches(value)) {
            return throwException();
        }
        return this;
    }

    public ValidationProperty isNotEqualTo(Integer value) {
        Integer propertyInt = isNumeric();
        if (propertyInt.equals(value)) {
            throwException();
        }
        return this;
    }

    public ValidationProperty minValue(Integer value) {
        Integer propertyInt = isNumeric();
        if (propertyInt < value) {
            throwException();
        }
        return this;
    }

    public ValidationProperty maxValue(Integer value) {
        Integer propertyInt = isNumeric();
        if (propertyInt > value) {
            throwException();
        }
        return this;
    }

    public ValidationProperty betweenValues(Integer value1, Integer value2) {
        Integer propertyInt = isNumeric();
        if (propertyInt < value1 || propertyInt > value2) {
            throwException();
        }
        return this;
    }

    public ValidationProperty minLength(Integer length) {
        if (propertyValue.length() < length) {
            throwException();
        }
        return this;
    }

    public ValidationProperty maxLength(Integer length) {
        if (propertyValue.length() > length) {
            throwException();
        }
        return this;
    }

    public ValidationProperty lengthBetween(Integer minLength, Integer maxLength) {
        if (propertyValue.length() < minLength || propertyValue.length() > maxLength) {
            throwException();
        }
        return this;
    }

    public ValidationProperty isNumber() {
        isNumeric();
        return this;
    }


    private Integer isNumeric() {
        if (propertyValue == null) {
            throwException();
        }
        try {
            double d = Double.parseDouble(propertyValue);
        } catch (NumberFormatException nfe) {
            throwException();
        }
        return Integer.parseInt(propertyValue);
    }

    private ValidationProperty throwException() {
        throw new SiemensPropertiesException();
    }
}
